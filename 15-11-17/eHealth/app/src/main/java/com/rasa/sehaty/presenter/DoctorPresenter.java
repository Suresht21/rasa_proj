package com.rasa.sehaty.presenter;

import android.util.Log;

import com.google.gson.JsonObject;
import com.rasa.sehaty.activities.SearchDoctorsScreen;
import com.rasa.sehaty.interactor.DoctorInteractor;

public class DoctorPresenter extends Presenter implements IDoctorPresenter {

    private static final String TAG = DoctorPresenter.class.getSimpleName();
    private DoctorInteractor doctorInteractor;
    private SearchDoctorsScreen searchDoctorsScreen;

    public DoctorPresenter(SearchDoctorsScreen searchDoctorsScreen, String token) {
        this.searchDoctorsScreen = searchDoctorsScreen;
        doctorInteractor = new DoctorInteractor(this, token);
    }

    @Override
    public void onDoctorsListRequest(String token) {
        doctorInteractor.onCallDoctorList();
    }

    @Override
    public void onSucessDoctorsList(JsonObject jsonObject) {
        Log.d(TAG, "Doctor List : " + jsonObject);
        searchDoctorsScreen.onSuccessResponse(jsonObject);
    }

    @Override
    public void onFailureDoctorsList(String message) {

    }
}
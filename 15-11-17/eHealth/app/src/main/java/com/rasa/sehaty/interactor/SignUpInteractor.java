package com.rasa.sehaty.interactor;

import com.google.gson.JsonObject;
import com.rasa.sehaty.interfaces.HealthRetrofitServices;
import com.rasa.sehaty.interfaces.ISignUpInteractor;
import com.rasa.sehaty.model.SignUpVo;
import com.rasa.sehaty.presenter.Presenter;
import com.rasa.sehaty.presenter.SignUpPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpInteractor extends Interactor implements ISignUpInteractor {

    private Presenter presenter;

    public SignUpInteractor(Presenter presenter) {
        super(presenter);
        this.presenter = presenter;
    }


    @Override
    public void onCallRegister(SignUpVo signUpVo) {
        Call<JsonObject> signUpCall = retrofit.create(HealthRetrofitServices.class).register(signUpVo);
        signUpCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    ((SignUpPresenter) presenter).onSuccessRegister(response.body());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
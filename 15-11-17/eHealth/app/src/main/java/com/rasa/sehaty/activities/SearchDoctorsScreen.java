package com.rasa.sehaty.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.JsonObject;
import com.rasa.sehaty.R;
import com.rasa.sehaty.adapters.DoctorsListAdapter;
import com.rasa.sehaty.databinding.ActivitySearchDoctorsScreenBinding;
import com.rasa.sehaty.databinding.ContentSearchDoctorsScreenBinding;
import com.rasa.sehaty.model.Doctor;
import com.rasa.sehaty.prefs.DoctorsInfo;
import com.rasa.sehaty.presenter.DoctorPresenter;
import com.rasa.sehaty.utils.Utils;

import java.util.ArrayList;

public class SearchDoctorsScreen extends AppCompatActivity implements View.OnClickListener {

    private ActivitySearchDoctorsScreenBinding activitySearchDoctorsScreenBinding;
    private ContentSearchDoctorsScreenBinding screenBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_doctors_screen);
        activitySearchDoctorsScreenBinding = (ActivitySearchDoctorsScreenBinding) viewDataBinding;
        setSupportActionBar(activitySearchDoctorsScreenBinding.toolbar);
        screenBinding = activitySearchDoctorsScreenBinding.contentSearchDocLayout;
        screenBinding.proceedButton.setOnClickListener(this);
        loadDoctorsList();
    }

    public void loadDoctorsList() {
        Utils.showProgessBar(this);
        String bearer_token = getIntent().getStringExtra("bearer_token");
        DoctorPresenter doctorPresenter = new DoctorPresenter(this, bearer_token);
        doctorPresenter.onDoctorsListRequest(bearer_token);
    }


    public void onSuccessResponse(JsonObject jsonObject) {
        Utils.hideProgessBar();
        DoctorsInfo doctorsInfo = new DoctorsInfo();
        doctorsInfo.addDoctorsList();
        ArrayList<Doctor> doctorArrayList = doctorsInfo.getDoctorArrayList();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        screenBinding.searchDoctorsList.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(screenBinding.searchDoctorsList.getContext(),
                linearLayoutManager.getOrientation());
        screenBinding.searchDoctorsList.addItemDecoration(dividerItemDecoration);
        screenBinding.searchDoctorsList.setAdapter(new DoctorsListAdapter(this, doctorArrayList));

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.proceed_button) {
            startActivity(new Intent(this, HealthToolsActivity.class));
        }
    }
}

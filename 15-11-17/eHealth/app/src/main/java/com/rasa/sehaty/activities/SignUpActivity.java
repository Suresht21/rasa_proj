package com.rasa.sehaty.activities;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import com.google.gson.JsonObject;
import com.rasa.sehaty.R;
import com.rasa.sehaty.databinding.ActivitySignUpBinding;
import com.rasa.sehaty.model.SignUpVo;
import com.rasa.sehaty.presenter.SignUpPresenter;
import com.rasa.sehaty.utils.Utils;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();
    private ActivitySignUpBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        binding = (ActivitySignUpBinding) viewDataBinding;
    }


    public void performRegister(View view) {
        if (!validateFields()) return;
        Utils.showProgessBar(this);
        SignUpPresenter signUpPresenter = new SignUpPresenter(this);
        SignUpVo signUpVo = new SignUpVo();
        signUpVo.setUserName(binding.userName.getText().toString());
        signUpVo.setPassword(binding.password.getText().toString());
        signUpVo.setEmailId(binding.emailId.getText().toString());
        signUpVo.setMobileNumber(binding.mobileNumber.getText().toString());
        signUpVo.setConfirmPassword(binding.confirmPassword.getText().toString());
        signUpVo.setRole(6);
        signUpPresenter.onRegisterRequest(signUpVo);
    }

    private boolean validateFields() {
        if (binding.userName.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter User Name.");
            return false;
        }
        if (binding.mobileNumber.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter Mobile Number.");
            return false;
        }

        if (binding.emailId.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter EmailID.");
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(binding.emailId.getText().toString()).matches()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter Valid EmailID.");
            return false;
        }

        if (binding.password.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter Password.");
            return false;
        }

        if (binding.confirmPassword.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter Confirm Password.");
            return false;
        }

        if (!binding.confirmPassword.getText().toString().equals(binding.password.getText().toString())) {
            Utils.showSnackBar(binding.getRoot(), "Password & Confirm Password Must Match.");
            return false;
        }
        return true;
    }

    public void onSuccessRegister(JsonObject jsonObject) {
        Utils.hideProgessBar();
        Log.d(TAG, "onSuccessResponse " + jsonObject);
        Utils.showSnackBar(binding.getRoot(), "Registration Successfully completed.");
        finish();
    }
}
package com.rasa.sehaty.prefs;

public interface IPreferencesHelper {
    String PREF_NAME = "shethy_pref";
    String SESSION_KEY = "session_id";
    String ACCESS_KEY = "access_token";
    String REFRESH_KEY = "refresh_token";

    void setSessionId(String sessionId);

    String getSessionId();

    void setAccessToken(String accessToken);

    String getAccessToken();


    void setRefreshToken(String refreshToken);

    String getRefreshToken();

}
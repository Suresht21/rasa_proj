package com.rasa.sehaty.presenter;

import com.google.gson.JsonObject;
import com.rasa.sehaty.model.LoginVo;

public interface ILoginPresenter {

    public void onLoginRequest(LoginVo loginVo);

    public void onSucessLogin(JsonObject jsonObject);

    public void onFailureLogin(String message);
}
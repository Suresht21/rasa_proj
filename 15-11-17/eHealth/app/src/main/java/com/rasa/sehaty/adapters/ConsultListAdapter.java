package com.rasa.sehaty.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rasa.sehaty.R;
import com.rasa.sehaty.activities.ProceedActivity;
import com.rasa.sehaty.databinding.ItemBookAppointmentTypeBinding;
import com.rasa.sehaty.model.ConsultType;
import com.rasa.sehaty.model.Doctor;

import java.util.ArrayList;

public class ConsultListAdapter extends RecyclerView.Adapter<ConsultListAdapter.ConsultHolder> {


    private static final String TAG = ConsultListAdapter.class.getSimpleName();
    private final Context context;
    private ArrayList<ConsultType> consultTypeArrayList;
    private Doctor doctor;

    public ConsultListAdapter(Context context, ArrayList<ConsultType> consultTypeArrayList, Doctor doctor) {
        this.context = context;
        this.consultTypeArrayList = consultTypeArrayList;
        this.doctor = doctor;
    }

    @Override
    public ConsultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_book_appointment_type, parent, false);
        ItemBookAppointmentTypeBinding binding = (ItemBookAppointmentTypeBinding) viewDataBinding;
        return new ConsultHolder(binding);
    }

    @Override
    public void onBindViewHolder(ConsultHolder holder, int position) {
        ConsultType consultType = consultTypeArrayList.get(position);
        holder.binding.consultName.setText(consultType.consultName);
        holder.binding.consultTypeButton.setText(consultType.buttonText);
        holder.binding.amount.setText(consultType.amount);
        holder.binding.typeImg.setImageResource(consultType.consultResource);
        holder.binding.validity.setText(consultType.validity);
    }

    @Override
    public int getItemCount() {
        return consultTypeArrayList.size();
    }

    class ConsultHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemBookAppointmentTypeBinding binding;

        public ConsultHolder(ItemBookAppointmentTypeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.consultTypeButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (binding.consultTypeButton.getId() == view.getId() && getAdapterPosition() == 2) {
                Intent intent = new Intent(context, ProceedActivity.class);
                Log.d(TAG, "Docotor Instance " + doctor);
                intent.putExtra("doctor", doctor);
                context.startActivity(intent);
            }
        }
    }
}
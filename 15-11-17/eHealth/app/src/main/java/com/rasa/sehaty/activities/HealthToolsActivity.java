package com.rasa.sehaty.activities;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.rasa.sehaty.R;
import com.rasa.sehaty.adapters.HealthToolsAdapter;
import com.rasa.sehaty.databinding.ActivityHealthToolsBinding;
import com.rasa.sehaty.databinding.CustomTitleBarBinding;
import com.rasa.sehaty.model.HealthTool;

import java.util.ArrayList;

public class HealthToolsActivity extends AppCompatActivity {

    private ActivityHealthToolsBinding binding;

    ArrayList<HealthTool> healthTools = new ArrayList<>();
    private CustomTitleBarBinding titleBarBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_health_tools);
        binding = (ActivityHealthToolsBinding) viewDataBinding;

        setupActionBar();
        HealthTool bmiTool = new HealthTool();
        bmiTool.setToolName("BMI Calculator");
        bmiTool.setToolResource(R.drawable.ic_bmi);
        healthTools.add(bmiTool);

        HealthTool bmrTool = new HealthTool();
        bmrTool.setToolName("BMR");
        bmrTool.setToolResource(R.drawable.ic_bmr);
        healthTools.add(bmrTool);

        HealthTool bfpTool = new HealthTool();
        bfpTool.setToolName("Body Fat Percentage");
        bfpTool.setToolResource(R.drawable.ic_body_fat);
        healthTools.add(bfpTool);

        HealthTool lbmTool = new HealthTool();
        lbmTool.setToolName("Lean Body Mass");
        lbmTool.setToolResource(R.drawable.ic_lean_body_mass);
        healthTools.add(lbmTool);

        HealthTool waterTool = new HealthTool();
        waterTool.setToolName("Water Intake");
        waterTool.setToolResource(R.drawable.ic_water_intake);
        healthTools.add(waterTool);

        HealthTool pTool = new HealthTool();
        pTool.setToolName("Pregnancy Due Date");
        pTool.setToolResource(R.drawable.ic_pregnancy);
        healthTools.add(pTool);

        setToolsList();
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_title_bar);

        View customView = getSupportActionBar().getCustomView();
        ViewDataBinding viewDataBinding = DataBindingUtil.bind(customView);
        titleBarBinding = (CustomTitleBarBinding) viewDataBinding;
        titleBarBinding.titleText.setText("Health Tools");
    }

    private void setToolsList() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        binding.toolsGrid.setHasFixedSize(true);
        binding.toolsGrid.setLayoutManager(gridLayoutManager);
        binding.toolsGrid.setAdapter(new HealthToolsAdapter(this, healthTools));
    }

}
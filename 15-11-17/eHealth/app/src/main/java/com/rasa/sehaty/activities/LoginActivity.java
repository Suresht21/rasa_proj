package com.rasa.sehaty.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.gson.JsonObject;
import com.rasa.sehaty.R;
import com.rasa.sehaty.databinding.ActivityLoginBinding;
import com.rasa.sehaty.model.LoginVo;
import com.rasa.sehaty.prefs.SharedPref;
import com.rasa.sehaty.presenter.LoginPresenter;
import com.rasa.sehaty.utils.Utils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLoginBinding binding;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding = (ActivityLoginBinding) viewDataBinding;
        binding.signUpText.setOnClickListener(this);
        sharedPref = SharedPref.getmSharedPrefInstance(this);
    }

    public void performLogin(View view) {
        if (!validateFields()) return;
        Utils.showProgessBar(this);
        LoginPresenter loginPresenter = new LoginPresenter(this);
        LoginVo loginVo = new LoginVo();
        loginVo.setUsername(binding.userName.getText().toString());
        loginVo.setPassword(binding.password.getText().toString());
        loginVo.setGrant_type("password");
        loginPresenter.onLoginRequest(loginVo);

    }

    public void performGoogleLogin(View view) {
        Utils.showSnackBar(binding.getRoot(), "Not Yet implemented!");
    }

    public void performFbLogin(View view) {
        Utils.showSnackBar(binding.getRoot(), "Not Yet implemented!");
    }


    private boolean validateFields() {
        if (binding.userName.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter User Name.");
            return false;
        }
        if (binding.password.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Enter Password.");
            return false;
        }
        return true;
    }


    public void onSuccessLogin(JsonObject jsonObject) {
        Utils.hideProgessBar();
        if (jsonObject.has("access_token")) {
            sharedPref.setAccessToken(jsonObject.get("access_token").getAsString());
        }

        if (jsonObject.has("userName")) {
        }

        if (jsonObject.has("refresh_token")) {
            sharedPref.setRefreshToken(jsonObject.get("refresh_token").getAsString());

        }

        Intent intent = new Intent(this, SearchDoctorsScreen.class);
        intent.putExtra("bearer_token", jsonObject.get("access_token").getAsString());
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.sign_up_text) {
            startActivity(new Intent(this, SignUpActivity.class));
        }
    }

    public void onFailureLogin(String message) {
        Utils.hideProgessBar();
        Utils.showSnackBar(binding.getRoot(), message);
    }
}
package com.rasa.sehaty.interfaces;

import com.google.gson.JsonObject;
import com.rasa.sehaty.model.SignUpVo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface HealthRetrofitServices {

    @FormUrlEncoded
    @POST("token")
    Call<JsonObject> login(@Field("grant_type") String grant_type, @Field("username") String username, @Field("password") String password);


    @POST("api/Account/Register")
    Call<JsonObject> register(@Body SignUpVo signUpVo);


    @GET("api/DoctorProfile/Get?isAll=true")
    Call<JsonObject> getDoctorsList();

}
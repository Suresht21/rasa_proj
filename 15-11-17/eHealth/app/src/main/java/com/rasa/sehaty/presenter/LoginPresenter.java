package com.rasa.sehaty.presenter;

import com.google.gson.JsonObject;
import com.rasa.sehaty.activities.LoginActivity;
import com.rasa.sehaty.interactor.LoginInteractor;
import com.rasa.sehaty.model.LoginVo;

public class LoginPresenter extends Presenter implements ILoginPresenter {

    private final LoginInteractor loginInteractor;
    private LoginActivity loginActivity;

    public LoginPresenter(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
        loginInteractor = new LoginInteractor(this);
    }


    @Override
    public void onLoginRequest(LoginVo loginVo) {
        loginInteractor.onCallLogin(loginVo);
    }

    @Override
    public void onSucessLogin(JsonObject jsonObject) {
        loginActivity.onSuccessLogin(jsonObject);
    }

    @Override
    public void onFailureLogin(String message) {
        loginActivity.onFailureLogin(message);
    }
}
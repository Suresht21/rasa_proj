package com.rasa.sehaty.interactor;

import com.google.gson.JsonObject;
import com.rasa.sehaty.interfaces.HealthRetrofitServices;
import com.rasa.sehaty.interfaces.IDoctorInteractor;
import com.rasa.sehaty.presenter.DoctorPresenter;
import com.rasa.sehaty.presenter.Presenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorInteractor extends Interactor implements IDoctorInteractor {

    private final Presenter presenter;
    private final String token;

    public DoctorInteractor(Presenter presenter, String token) {
        super(presenter, token);
        this.presenter = presenter;
        this.token = token;
    }

    @Override
    public void onCallDoctorList() {
        Call<JsonObject> doctorsList = retrofit.create(HealthRetrofitServices.class).getDoctorsList();
        doctorsList.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                ((DoctorPresenter) presenter).onSucessDoctorsList(response.body());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
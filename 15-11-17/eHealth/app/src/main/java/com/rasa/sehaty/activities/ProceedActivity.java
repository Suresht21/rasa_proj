package com.rasa.sehaty.activities;

import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.rasa.sehaty.R;
import com.rasa.sehaty.databinding.CustomTitleBarBinding;
import com.rasa.sehaty.databinding.ProceedLayoutBinding;
import com.rasa.sehaty.model.Doctor;
import com.rasa.sehaty.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

public class ProceedActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = ProceedActivity.class.getSimpleName();
    private ProceedLayoutBinding binding;
    private CustomTitleBarBinding titleBarBinding;
    private Doctor doctor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.proceed_layout);
        setupActionBar();

        binding = (ProceedLayoutBinding) viewDataBinding;
        binding.calendarIcon.setOnClickListener(this);
        binding.timeIcon.setOnClickListener(this);

        doctor = (Doctor) getIntent().getSerializableExtra("doctor");

        Log.d(TAG, "Docotor Instance " + doctor);

        titleBarBinding.titleText.setText(doctor.doctorName);
        titleBarBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.proceedButton.setOnClickListener(this);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_title_bar);

        View customView = getSupportActionBar().getCustomView();
        ViewDataBinding viewDataBinding = DataBindingUtil.bind(customView);
        titleBarBinding = (CustomTitleBarBinding) viewDataBinding;
    }

    public void showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                ProceedActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOkColor(getResources().getColor(R.color.orange_color));
        dpd.setCancelColor(getResources().getColor(R.color.orange_color));
        dpd.setAccentColor(getResources().getColor(R.color.orange_color));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        binding.selectDateEdit.setText("" + dayOfMonth + "/" + (++monthOfYear) + "/" + year);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.calendar_icon) {
            showDatePicker();
        } else if (view.getId() == R.id.time_icon) {
            showTimePicker();
        } else if (view.getId() == R.id.proceed_button) {
            if (!validateFields()) return;
            showAppointmentPopup();
        }
    }

    private void showAppointmentPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.appointment_success_layout);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText("Your appointment is booked with " + doctor.doctorName + " on " + binding.selectDateEdit.getText().toString()
                + ", " + binding.selectTimeEdit.getText().toString());

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private boolean validateFields() {
        if (binding.selectDateEdit.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Select Appointment Date.");
            return false;
        }

        if (binding.selectTimeEdit.getText().toString().isEmpty()) {
            Utils.showSnackBar(binding.getRoot(), "Please Select Appointment Time.");
            return false;
        }
        return true;
    }

    private void showTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                ProceedActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.setTimeInterval(1, 30);
        tpd.show(getFragmentManager(), "Timepickerdialog");
        tpd.setOkColor(getResources().getColor(R.color.orange_color));
        tpd.setCancelColor(getResources().getColor(R.color.orange_color));
        tpd.setAccentColor(getResources().getColor(R.color.orange_color));
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        binding.selectTimeEdit.setText(hourString + " : " + minuteString + "");
    }
}
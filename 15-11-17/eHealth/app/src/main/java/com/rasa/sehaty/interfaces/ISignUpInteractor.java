package com.rasa.sehaty.interfaces;


import com.rasa.sehaty.model.SignUpVo;

public interface ISignUpInteractor {
    public void onCallRegister(SignUpVo signUpVo);
}
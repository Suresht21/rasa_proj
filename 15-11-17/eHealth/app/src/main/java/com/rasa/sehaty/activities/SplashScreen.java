package com.rasa.sehaty.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.rasa.sehaty.R;
import com.rasa.sehaty.activities.LoginActivity;
import com.rasa.sehaty.activities.SearchDoctorsScreen;
import com.rasa.sehaty.prefs.SharedPref;

public class SplashScreen extends AppCompatActivity {

    private SharedPref sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startHome();
        sharedPref = SharedPref.getmSharedPrefInstance(this);
    }

    private void startHome() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (sharedPref.getAccessToken().equals("")) {

                    intent = new Intent(SplashScreen.this, LoginActivity.class);
                } else {
                    intent = new Intent(SplashScreen.this, SearchDoctorsScreen.class);
                    intent.putExtra("bearer_token", sharedPref.getAccessToken());
                }
                startActivity(intent);
                finish();
            }
        }, 3 * 1000);
    }
}

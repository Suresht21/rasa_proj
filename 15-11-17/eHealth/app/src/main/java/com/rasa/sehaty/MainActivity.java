package com.rasa.sehaty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.rasa.sehaty.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

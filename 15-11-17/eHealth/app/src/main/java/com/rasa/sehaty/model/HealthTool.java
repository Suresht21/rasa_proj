package com.rasa.sehaty.model;

import org.parceler.Parcel;

@Parcel
public class HealthTool {
    public String toolName;
    public int toolResource;

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public int getToolResource() {
        return toolResource;
    }

    public void setToolResource(int toolResource) {
        this.toolResource = toolResource;
    }


    @Override
    public String toString() {
        return "HealthTool{" +
                "toolName='" + toolName + '\'' +
                ", toolResource=" + toolResource +
                '}';
    }
}
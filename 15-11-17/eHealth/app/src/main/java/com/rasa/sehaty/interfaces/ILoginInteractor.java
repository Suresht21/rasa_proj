package com.rasa.sehaty.interfaces;


import com.rasa.sehaty.model.LoginVo;

public interface ILoginInteractor {
    public void onCallLogin(LoginVo loginVo);
}
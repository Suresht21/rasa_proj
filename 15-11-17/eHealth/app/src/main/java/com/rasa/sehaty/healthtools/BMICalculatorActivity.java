package com.rasa.sehaty.healthtools;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.rasa.sehaty.R;
import com.rasa.sehaty.databinding.HealthToolCalcLayoutBinding;
import com.rasa.sehaty.model.HealthTool;

import org.parceler.Parcels;

public class BMICalculatorActivity extends AppCompatActivity {
    HealthToolCalcLayoutBinding binding;
    private HealthTool healthTool;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.health_tool_calc_layout);
        binding = (HealthToolCalcLayoutBinding) viewDataBinding;
        healthTool = Parcels.unwrap(getIntent().getParcelableExtra("health_tool"));

        if (healthTool.getToolName().equals("BMI Calculator")) {
            handleBMICalculator();
        } else if (healthTool.getToolName().equals("BMR")) {
            handleBMR();
        } else if (healthTool.getToolName().equals("Body Fat Percentage")) {
            handleBFP();
        } else if (healthTool.getToolName().equals("Lean Body Mass")) {
            handleLBM();
        } else if (healthTool.getToolName().equals("Water Intake")) {
            handleWaterIntake();
        } else if (healthTool.getToolName().equals("Pregnancy Due Date")) {
            handlePDD();
        }

    }

    private void handlePDD() {
        binding.activityLevelLayout.setVisibility(View.GONE);
        binding.ageLayout.setVisibility(View.GONE);
    }

    private void handleWaterIntake() {
        binding.weightOrLayout.setVisibility(View.GONE);
        binding.orHeightLayout.setVisibility(View.GONE);
        binding.ageLayout.setVisibility(View.GONE);
        binding.pddLayout.setVisibility(View.GONE);
    }

    private void handleLBM() {
        binding.activityLevelLayout.setVisibility(View.GONE);
        binding.weightOrLayout.setVisibility(View.GONE);
        binding.orHeightLayout.setVisibility(View.GONE);
        binding.pddLayout.setVisibility(View.GONE);
    }

    private void handleBFP() {
        binding.weightOrLayout.setVisibility(View.GONE);
        binding.orHeightLayout.setVisibility(View.GONE);
        binding.activityLevelLayout.setVisibility(View.GONE);
        binding.pddLayout.setVisibility(View.GONE);
    }

    private void handleBMR() {
        binding.weightOrLayout.setVisibility(View.GONE);
        binding.orHeightLayout.setVisibility(View.GONE);
        binding.pddLayout.setVisibility(View.GONE);
    }

    private void handleBMICalculator() {
        binding.genderRadioGrp.setVisibility(View.GONE);
        binding.ageLayout.setVisibility(View.GONE);
        binding.heightFtInLayout.setVisibility(View.GONE);
        binding.orHeightText.setVisibility(View.GONE);
        binding.activityLevelLayout.setVisibility(View.GONE);
        binding.pddLayout.setVisibility(View.GONE);
    }

}
package com.rasa.sehaty.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.rasa.sehaty.R;
import com.rasa.sehaty.activities.DoctorsDetailScreen;
import com.rasa.sehaty.databinding.ItemDoctorsListBinding;
import com.rasa.sehaty.model.Doctor;

import java.util.ArrayList;

public class DoctorsListAdapter extends RecyclerView.Adapter<DoctorsListAdapter.DoctorHolder> {


    private final Context context;
    private final ArrayList<Doctor> doctorArrayList;

    public DoctorsListAdapter(Context context, ArrayList<Doctor> doctorArrayList) {
        this.context = context;
        this.doctorArrayList = doctorArrayList;
    }

    @Override
    public DoctorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_doctors_list, parent, false);
        ItemDoctorsListBinding binding = (ItemDoctorsListBinding) viewDataBinding;
        return new DoctorHolder(binding);
    }

    @Override
    public void onBindViewHolder(DoctorHolder holder, int position) {
        Doctor doctor = doctorArrayList.get(position);
        holder.binding.doctorName.setText(doctor.doctorName);
        holder.binding.roleExp.setText(doctor.speciality + " " + doctor.doctorsExpYears);
        holder.binding.doctorDesc.setText(doctor.about_doctor);
        holder.binding.percentage.setText(doctor.rating);
        holder.binding.eliteIcon.setVisibility(doctor.elite ? View.VISIBLE : View.INVISIBLE);

        if (doctor.doctorImageName.equals("doctor_one")) {
            holder.binding.doctorImg.setImageResource(R.drawable.doctor_one);
        } else if (doctor.doctorImageName.equals("doctor_two")) {
            holder.binding.doctorImg.setImageResource(R.drawable.doctor_two);
        } else if (doctor.doctorImageName.equals("doctor_three")) {
            holder.binding.doctorImg.setImageResource(R.drawable.doctor_three);
        } else if (doctor.doctorImageName.equals("doctor_four")) {
            holder.binding.doctorImg.setImageResource(R.drawable.doctor_four);
        } else if (doctor.doctorImageName.equals("doctor_five")) {
            holder.binding.doctorImg.setImageResource(R.drawable.doctor_five);
        } else {
            holder.binding.doctorImg.setImageResource(R.mipmap.ic_launcher);
        }
    }

    @Override
    public int getItemCount() {
        return doctorArrayList.size();
    }

    class DoctorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemDoctorsListBinding binding;

        public DoctorHolder(ItemDoctorsListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, DoctorsDetailScreen.class);
            intent.putExtra("doctor", doctorArrayList.get(getAdapterPosition()));
            context.startActivity(intent);
        }
    }
}
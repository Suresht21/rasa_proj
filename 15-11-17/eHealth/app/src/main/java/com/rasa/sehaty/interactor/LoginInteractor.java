package com.rasa.sehaty.interactor;

import android.util.Log;

import com.google.gson.JsonObject;
import com.rasa.sehaty.interactor.Interactor;
import com.rasa.sehaty.interfaces.HealthRetrofitServices;
import com.rasa.sehaty.interfaces.ILoginInteractor;
import com.rasa.sehaty.model.LoginVo;
import com.rasa.sehaty.presenter.LoginPresenter;
import com.rasa.sehaty.presenter.Presenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractor extends Interactor implements ILoginInteractor {

    private static final String TAG = LoginInteractor.class.getSimpleName();
    private Presenter presenter;

    public LoginInteractor(Presenter presenter) {
        super(presenter);
        this.presenter = presenter;
    }

    @Override
    public void onCallLogin(LoginVo loginVo) {
        Call<JsonObject> loginCall = retrofit.create(HealthRetrofitServices.class).login(loginVo.getGrant_type(), loginVo.getUsername(), loginVo.getPassword());
        loginCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    ((LoginPresenter) presenter).onSucessLogin(response.body());
                } else {
                    Log.d(TAG, "Login Response");
                    ((LoginPresenter) presenter).onFailureLogin("The user name or password is incorrect.");
//                    if (response.body().has("error")) {
//                        ((LoginPresenter) presenter).onFailureLogin(response.body().get("error_description").getAsString());
//                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
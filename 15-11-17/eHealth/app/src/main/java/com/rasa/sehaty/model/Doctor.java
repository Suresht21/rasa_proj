package com.rasa.sehaty.model;


import java.io.Serializable;

public class Doctor implements Serializable {
    public String doctorName;
    public String speciality;
    public String qualification;
    public String rating;
    public String about_doctor;
    public String doctorAvailDays;
    public String doctorAvailTime;
    public String doctorsExpYears;
    public String doctorExps;
    public String awards;
    public boolean elite;
    public String specializations;
    public String doctorImageName;
    public String registration;
    public String memberShip;

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorName='" + doctorName + '\'' +
                ", speciality='" + speciality + '\'' +
                ", qualification='" + qualification + '\'' +
                ", rating='" + rating + '\'' +
                ", about_doctor='" + about_doctor + '\'' +
                ", doctorAvailDays='" + doctorAvailDays + '\'' +
                ", doctorAvailTime='" + doctorAvailTime + '\'' +
                ", doctorsExpYears='" + doctorsExpYears + '\'' +
                ", doctorExps='" + doctorExps + '\'' +
                ", awards='" + awards + '\'' +
                ", elite=" + elite +
                ", specializations='" + specializations + '\'' +
                ", doctorImageName='" + doctorImageName + '\'' +
                ", registration='" + registration + '\'' +
                ", memberShip='" + memberShip + '\'' +
                '}';
    }
}
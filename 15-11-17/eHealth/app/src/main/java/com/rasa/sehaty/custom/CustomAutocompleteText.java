package com.rasa.sehaty.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomAutocompleteText extends AppCompatAutoCompleteTextView {

    private Context context;

    public CustomAutocompleteText(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Raleway-Regular.ttf");
            setTypeface(tf);
        }
    }

    public CustomAutocompleteText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomAutocompleteText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


}
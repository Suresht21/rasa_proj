package com.rasa.sehaty.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref implements IPreferencesHelper {

    public static SharedPref mSharedPref;
    private SharedPreferences sharedPreferences;

    private SharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences(IPreferencesHelper.PREF_NAME, Context.MODE_PRIVATE);
    }


    public static SharedPref getmSharedPrefInstance(Context context) {
        if (mSharedPref == null)
            mSharedPref = new SharedPref(context);
        return mSharedPref;
    }


    @Override
    public void setSessionId(String sessionId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SESSION_KEY, sessionId);
        editor.apply();
    }

    @Override
    public String getSessionId() {
        return sharedPreferences.getString(SESSION_KEY, "");
    }

    @Override
    public void setAccessToken(String accessToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_KEY, accessToken);
        editor.apply();
    }

    @Override
    public String getAccessToken() {
        return sharedPreferences.getString(ACCESS_KEY, "");
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REFRESH_KEY, refreshToken);
        editor.apply();
    }

    @Override
    public String getRefreshToken() {
        return sharedPreferences.getString(REFRESH_KEY, "");
    }
}
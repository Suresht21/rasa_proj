package com.rasa.sehaty.activities;

import android.content.ContentUris;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.rasa.sehaty.R;
import com.rasa.sehaty.databinding.CustomTitleBarBinding;
import com.rasa.sehaty.databinding.DoctorDetailsLayoutBinding;
import com.rasa.sehaty.model.Doctor;

import java.io.Serializable;

public class DoctorsDetailScreen extends AppCompatActivity implements View.OnClickListener {

    private DoctorDetailsLayoutBinding binding;
    private Doctor doctor;
    private CustomTitleBarBinding titleBarBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.doctor_details_layout);
        setupActionBar();
        binding = (DoctorDetailsLayoutBinding) viewDataBinding;
        doctor = (Doctor) getIntent().getSerializableExtra("doctor");

        binding.aboutDescText.setText(doctor.about_doctor);
        binding.specializationsDescText.setText(doctor.specializations);
        binding.educationDescText.setText(doctor.qualification);
        binding.experienceDescText.setText(doctor.doctorExps);
        binding.percentage.setText(doctor.rating);
        binding.doctorName.setText(doctor.doctorName);
        binding.specialityValueText.setText(doctor.speciality);
        binding.expValueText.setText(doctor.doctorsExpYears);
        binding.weekDays.setText(doctor.doctorAvailDays);
        binding.timeText.setText(doctor.doctorAvailTime);
        binding.memberShipDescText.setText(doctor.memberShip);
        binding.registrationDescText.setText(doctor.registration);

        binding.bookAppointmentButton.setOnClickListener(this);

        if (doctor.doctorImageName.equals("doctor_one")) {
            binding.doctorImg.setImageResource(R.drawable.doctor_one);
        } else if (doctor.doctorImageName.equals("doctor_two")) {
            binding.doctorImg.setImageResource(R.drawable.doctor_two);
        } else if (doctor.doctorImageName.equals("doctor_three")) {
            binding.doctorImg.setImageResource(R.drawable.doctor_three);
        } else if (doctor.doctorImageName.equals("doctor_four")) {
            binding.doctorImg.setImageResource(R.drawable.doctor_four);
        } else if (doctor.doctorImageName.equals("doctor_five")) {
            binding.doctorImg.setImageResource(R.drawable.doctor_five);
        } else {
            binding.doctorImg.setImageResource(R.mipmap.ic_launcher);
        }

        titleBarBinding.titleText.setText(doctor.doctorName);

        titleBarBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private void setupActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_title_bar);

        View customView = getSupportActionBar().getCustomView();
        ViewDataBinding viewDataBinding = DataBindingUtil.bind(customView);
        titleBarBinding = (CustomTitleBarBinding) viewDataBinding;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, BookAppointmentTypeActivity.class);
        intent.putExtra("doctor", doctor);
        startActivity(intent);
    }
}
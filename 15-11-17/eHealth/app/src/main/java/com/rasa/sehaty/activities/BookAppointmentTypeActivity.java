package com.rasa.sehaty.activities;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;


import com.rasa.sehaty.R;
import com.rasa.sehaty.adapters.ConsultListAdapter;
import com.rasa.sehaty.databinding.BookAppointmentTypesLayoutBinding;
import com.rasa.sehaty.databinding.CustomTitleBarBinding;
import com.rasa.sehaty.model.ConsultType;
import com.rasa.sehaty.model.Doctor;

import java.io.Serializable;
import java.util.ArrayList;

public class BookAppointmentTypeActivity extends AppCompatActivity {

    private static final String TAG = BookAppointmentTypeActivity.class.getSimpleName();
    private BookAppointmentTypesLayoutBinding binding;
    private CustomTitleBarBinding titleBarBinding;
    private Doctor doctor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.book_appointment_types_layout);
        setupActionBar();
        binding = (BookAppointmentTypesLayoutBinding) viewDataBinding;
        doctor = (Doctor) getIntent().getSerializableExtra("doctor");
        setConsultTypes();

        titleBarBinding.titleText.setText(doctor.doctorName);
        titleBarBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void setConsultTypes() {
        ArrayList<ConsultType> consultTypeArrayList = new ArrayList<>();

        ConsultType chatConsultType = new ConsultType();
        chatConsultType.consultName = "Text Consulting";
        chatConsultType.validity = "Valid for 1 week";
        chatConsultType.consultResource = R.drawable.text_consult_icon;
        chatConsultType.buttonText = "Consult";
        chatConsultType.amount = "Riyal 50";

        consultTypeArrayList.add(chatConsultType);

        ConsultType callConsultType = new ConsultType();
        callConsultType.consultName = "Call Consulting";
        callConsultType.validity = "Duration 20min";
        callConsultType.buttonText = "Call";
        callConsultType.consultResource = R.drawable.call_consult_icon;
        callConsultType.amount = "Riyal 70";

        consultTypeArrayList.add(callConsultType);

        ConsultType bookConsultType = new ConsultType();
        bookConsultType.consultName = "Book Appointment";
        bookConsultType.validity = "Choose your flexible time";
        bookConsultType.buttonText = "Book";
        bookConsultType.consultResource = R.drawable.book_appointment_icon;
        bookConsultType.amount = "Riyal 70";

        consultTypeArrayList.add(bookConsultType);

        ConsultType homeVisitConsultType = new ConsultType();
        homeVisitConsultType.consultName = "Home Visit";
        homeVisitConsultType.validity = "Choose your flexible time";
        homeVisitConsultType.consultResource = R.drawable.home_visit_icon;
        homeVisitConsultType.buttonText = "Book";
        homeVisitConsultType.amount = "Riyal 70";

        consultTypeArrayList.add(homeVisitConsultType);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.bookAppointmentTypes.setLayoutManager(linearLayoutManager);
        Log.d(TAG, "Doctor instance " + doctor);
        ConsultListAdapter consultListAdapter = new ConsultListAdapter(this, consultTypeArrayList, doctor);
        binding.bookAppointmentTypes.setAdapter(consultListAdapter);

    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_title_bar);

        View customView = getSupportActionBar().getCustomView();
        ViewDataBinding viewDataBinding = DataBindingUtil.bind(customView);
        titleBarBinding = (CustomTitleBarBinding) viewDataBinding;
    }


}
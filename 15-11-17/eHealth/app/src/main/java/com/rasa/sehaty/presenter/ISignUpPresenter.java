package com.rasa.sehaty.presenter;

import com.google.gson.JsonObject;
import com.rasa.sehaty.model.SignUpVo;

public interface ISignUpPresenter {
    public void onRegisterRequest(SignUpVo loginVo);

    public void onSuccessRegister(JsonObject jsonObject);

    public void onFailureRegister(String message);
}
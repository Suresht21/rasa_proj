package com.rasa.sehaty.presenter;

import com.google.gson.JsonObject;

public interface IDoctorPresenter {

    public void onDoctorsListRequest(String token);

    public void onSucessDoctorsList(JsonObject jsonObject);

    public void onFailureDoctorsList(String message);
}
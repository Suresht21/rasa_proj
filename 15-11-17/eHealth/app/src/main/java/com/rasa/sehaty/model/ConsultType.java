package com.rasa.sehaty.model;

public class ConsultType {

    public String consultName;
    public int consultResource;
    public String buttonText;
    public String validity;
    public String amount;
}
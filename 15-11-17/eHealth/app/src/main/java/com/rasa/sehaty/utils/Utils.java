package com.rasa.sehaty.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.app.ProgressDialog;

public class Utils {

    private static ProgressDialog progressBar;

    public static void showSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }


    public static void showProgessBar(Context context) {
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(false);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
    }


    public static void hideProgessBar() {
        if (progressBar != null && progressBar.isShowing()) progressBar.dismiss();
    }
}
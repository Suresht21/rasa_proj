package com.rasa.sehaty.adapters;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rasa.sehaty.R;
import com.rasa.sehaty.activities.HealthToolActivity;
import com.rasa.sehaty.activities.HealthToolsActivity;
import com.rasa.sehaty.databinding.HealthToolItemBinding;
import com.rasa.sehaty.healthtools.PDDCalculatorActivity;
import com.rasa.sehaty.model.HealthTool;

import org.parceler.Parcels;

import java.util.ArrayList;

public class HealthToolsAdapter extends RecyclerView.Adapter<HealthToolsAdapter.ToolsHolder> {

    private final Activity activity;
    private final ArrayList<HealthTool> healthTools;

    public HealthToolsAdapter(Activity activity, ArrayList<HealthTool> healthTools) {
        this.activity = activity;
        this.healthTools = healthTools;
    }

    @Override
    public ToolsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.health_tool_item, parent, false);
        HealthToolItemBinding binding = (HealthToolItemBinding) viewDataBinding;
        return new ToolsHolder(binding);
    }

    @Override
    public void onBindViewHolder(ToolsHolder holder, int position) {
        HealthTool healthTool = healthTools.get(position);
        holder.binding.tabTitle.setText(healthTool.getToolName());
        holder.binding.tabIcon.setImageResource(healthTool.getToolResource());
    }

    @Override
    public int getItemCount() {
        return healthTools.size();
    }

    class ToolsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private HealthToolItemBinding binding;

        public ToolsHolder(HealthToolItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            HealthTool healthTool = healthTools.get(getAdapterPosition());
            if (healthTool.getToolName().equals("Pregnancy Due Date")) {
                Intent intent = new Intent(activity, PDDCalculatorActivity.class);
                intent.putExtra("health_tool", Parcels.wrap(healthTool));
                activity.startActivity(intent);
            } else {
                Intent intent = new Intent(activity, HealthToolActivity.class);
                intent.putExtra("health_tool", Parcels.wrap(healthTool));
                activity.startActivity(intent);
            }
        }
    }
}
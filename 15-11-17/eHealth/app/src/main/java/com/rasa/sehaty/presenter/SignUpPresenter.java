package com.rasa.sehaty.presenter;

import com.google.gson.JsonObject;
import com.rasa.sehaty.activities.SignUpActivity;
import com.rasa.sehaty.interactor.SignUpInteractor;
import com.rasa.sehaty.model.SignUpVo;

public class SignUpPresenter extends Presenter implements ISignUpPresenter {

    private final SignUpInteractor signUpInteractor;
    private SignUpActivity signUpActivity;

    public SignUpPresenter(SignUpActivity signUpActivity) {
        this.signUpActivity = signUpActivity;
        signUpInteractor = new SignUpInteractor(this);
    }


    @Override
    public void onRegisterRequest(SignUpVo signUpVo) {
        signUpInteractor.onCallRegister(signUpVo);
    }

    @Override
    public void onSuccessRegister(JsonObject jsonObject) {
        signUpActivity.onSuccessRegister(jsonObject);
    }

    @Override
    public void onFailureRegister(String message) {

    }
}
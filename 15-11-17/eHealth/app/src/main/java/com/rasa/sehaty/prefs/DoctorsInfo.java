package com.rasa.sehaty.prefs;


import com.rasa.sehaty.model.Doctor;

import java.util.ArrayList;

public class DoctorsInfo {
    ArrayList<Doctor> doctorArrayList = new ArrayList<>();

    public void addDoctorsList() {
        Doctor doctorOne = new Doctor();
        doctorOne.doctorName = "Dr. Suhail Bin Ahmed";
        doctorOne.speciality = "General Physician";
        doctorOne.qualification = "MBBS - Dr. NTR University of Health Sciences Andhra Pradesh, 2002, MD - General Medicine - Dr. NTR University of Health Sciences Andhra Pradesh, 2009, Post Graduate Diploma in Diabetology (PGDD)(Middlesex University, UK) - Middlesex University, London, UK, 2017 ";
        doctorOne.about_doctor = "Dr. Suhail Bin Ahmed is a Consultant Physician and Diabetologist in Tolichowki, Hyderabad and has an experience of 15 years in these fields. Dr. Suhail Bin Ahmed practices at Life Prime Clinics in Tolichowki, Hyderabad and Apollo Sugar Clinic - Tolichowki in Tolichowki, Hyderabad. He completed MBBS from NTR University of Health Sciences in 2002 and MD - General Medicine from NTR University of Health Sciences in 2009. He is a member of Indian Society of Emergency Medicine. Some of the services provided by the doctor are Health Checkup (General), Infectious Disease Treatment, Diabetes Management, Fever Treatment and skin checks etc. ";
        doctorOne.doctorAvailDays = "MON - SAT";
        doctorOne.doctorAvailTime = "8:00 PM - 10:00 PM";
        doctorOne.specializations = "Consultant, Physician Internal Medicine, Diabetologist ";
        doctorOne.doctorsExpYears = "16 Years";
        doctorOne.doctorExps = "As a Consultant at Care Hospital Banjara Hills";
        doctorOne.registration = "48074 Andhra Pradesh Medical Council, 2002";
        doctorOne.memberShip = "Indian Society of Emergency Medicine";
        doctorOne.elite = true;
        doctorOne.rating = "79%";
        doctorOne.doctorImageName = "doctor_one";
        doctorArrayList.add(doctorOne);

        Doctor doctorTwo = new Doctor();
        doctorTwo.doctorName = "Dr. Shaik Afroz";
        doctorTwo.speciality = "Dentist";
        doctorTwo.qualification = "BDS - Lenora Institute of Dental Sciences, 2008 F.A.G.E - Manipal - Karnataka, 2011";
        doctorTwo.about_doctor = "Dr. Shaik Afroz is a Dentist, Endodontist, and Cosmetic/Aesthetic Dentist in KPHB, Hyderabad and has an experience of 11 years in these fields.";
        doctorTwo.doctorAvailDays = "MON - SAT";
        doctorTwo.rating = "90%";
        doctorTwo.doctorAvailTime = "5:00 PM - 9:00 PM";
        doctorTwo.specializations = "Dentist";
        doctorTwo.doctorsExpYears = "11 Years";
        doctorTwo.awards = "FAGE";
        doctorTwo.doctorExps = "2010 - Present Owner at Neelofar Dental Care";
        doctorTwo.registration = "A07987 Andhra Pradesh State Dental Council, 2010";
        doctorTwo.memberShip = "Indian Dental Association";
        doctorTwo.elite = true;
        doctorTwo.doctorImageName = "doctor_two";

        doctorArrayList.add(doctorTwo);

        Doctor doctorThree = new Doctor();

        doctorThree.doctorName = "Dr. Mohammed Basheeruddin";
        doctorThree.speciality = "Dermatologist";
        doctorThree.qualification = "MBBS, Diploma in Dermatology, Dermatologist, Trichologist, Pediatric Dermatologist,";
        doctorThree.about_doctor = "Dr. Mohammed Basheeruddin is a consultant cosmetic dermatologist and trichologist at Derma Clinic-Skin Hair and Nail Care located at Tolichowki and Mallepally. Treating patients now for more than 13 years with placing medical ethics and patient care above the present trend of clinic commerce. His primary fields of interest are all hair related problems in men and women, psoriasis, vitiligo and pediatric dermatology. He also expertise in all cosmetic procedures including chemical peeling, laser hair reduction, laser scar reduction.";
        doctorThree.doctorAvailDays = "MON, WED, FRI";
        doctorThree.doctorAvailTime = "10:00 AM - 2:00 PM";
        doctorThree.rating = "91%";
        doctorThree.specializations = "Dermatologist, Trichologist, Pediatric Dermatologist";
        doctorThree.doctorsExpYears = "13 Years";
        doctorThree.doctorExps = "2003 - 2016 Consultant Dermatoloigst at Derma Clinic, 2003 - 2016 Consultant Dermatoloigst at Derma Clinic-Skin Hair & Nail Care";
        doctorThree.registration = "43205 Andhra Pradesh Medical Council, 1999";
        doctorThree.memberShip = "Indian Association of Dermatologists, Venereologists and Leprologists (IADVL), Cosmetic Dermatology Society of India (CDSI), Society of Pediatric Dermatology";
        doctorThree.elite = false;
        doctorThree.doctorImageName = "doctor_three";


        doctorArrayList.add(doctorThree);


        Doctor doctorFour = new Doctor();
        doctorFour.doctorName = "Dr. Syed Mohammed Ghouse.";
        doctorFour.speciality = "Urologist";
        doctorFour.qualification = "MBBS, MS - General Surgery, DNB - Urology/Genito - Urinary Surgery";
        doctorFour.about_doctor = "Dr. Syed Mohammed Ghouse is a Urologist Empanelled at the Asian Institute of Nephrology & Urology in Hyderabad. He has specialized knowledge and skill regarding problems of the urinary tracts and reproductive organs having a good clinical experience in dealing with the same. The treatments handled by Dr. Ghouse are regrading enlarged prostrate, cancers of the urinary tracts, infertility in Men and Women, intestinal Cystitis, Kidney Stones, Overactive Bladder, Urinary tract infections and bladder problems. He completed his DNB and MS in General Surgery after his MBBS and has gained a good expertise from his career in his field of work.";
        doctorFour.doctorAvailDays = "MON - SAT";
        doctorFour.rating = "95%";
        doctorFour.doctorAvailTime = "9:30 AM - 5:00 PM";
        doctorFour.specializations = "Urologist";
        doctorFour.doctorsExpYears = "7 Years";
        doctorFour.doctorExps = "2013 - Present Asst. Professor at Deccan Institute of Medical Sciences, 2013 - Present Resident at Deccan Institute of Medical Sciences, 2014 - Present Consultant Urologist at Yashoda Hospitals, 2014 - Present Urologist at Asian Institute Of Nephrology and Urology";
        doctorFour.registration = "67336 Andhra Pradesh Medical Council, 2010";
        doctorFour.awards = "Completed Certificate Course on Hospital Epidemiology & Infection at Apollo Hospital - 2008, Certified Basic Life Support & Advanced Cardiac Life Support Provider by American Heart Association (AHA) - 2006, Authored a Journal article 'Gossipyboma' for Indian Journal of Surgery - 2009";
        doctorFour.memberShip = "Indian Medical Association (IMA), Urological Society of India (USI), Andhra Pradesh Medical Council";
        doctorFour.elite = true;
        doctorFour.doctorImageName = "doctor_four";
        doctorArrayList.add(doctorFour);

        Doctor doctorFive = new Doctor();

        doctorFive.doctorName = "Dr. Mohd Rameez Isani";
        doctorFive.speciality = "Dentist";
        doctorFive.qualification = "BDS - M.A. Rangoonwala College of Dental Sciences Research Centre, 2010";
        doctorFive.about_doctor = "Dr. Mohd Rameez Isani is a Dentist and Implantologist in Red Hills, Hyderabad and has an experience of 7 years in these fields. Dr. Mohd Rameez Isani practices at City Dental Hospital in Red Hills, Hyderabad, Pearl Dental Clinic in Amberpet, Hyderabad and Dentzz Care Dental Studio in Vidyanagar, Hyderabad. He completed BDS from M.A. Rangoonwala College of Dental Sciences Research Centre in 2010.";
        doctorFive.doctorAvailDays = "MON - THU";
        doctorFive.doctorAvailTime = "10:00 AM - 2:00 PM";
        doctorFive.rating = "89%";
        doctorFive.specializations = "Dentist, Implantologist";
        doctorFive.doctorsExpYears = "7 Years";
        doctorFive.doctorExps = "2011 - 2013 consultant dentist at smile dental clinic";
        doctorFive.registration = "A 11463 Andhra Pradesh State Dental Council, 2012";
        doctorFive.awards = "Topper of his graduation batch - 2010";
        doctorFive.memberShip = "Indian Dental Association";
        doctorFive.elite = true;
        doctorFive.doctorImageName = "doctor_five";

        doctorArrayList.add(doctorFive);


    }

    public ArrayList<Doctor> getDoctorArrayList() {
        return doctorArrayList;
    }
}
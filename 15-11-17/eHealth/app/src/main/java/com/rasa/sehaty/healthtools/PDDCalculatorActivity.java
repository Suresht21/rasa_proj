package com.rasa.sehaty.healthtools;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.rasa.sehaty.R;
import com.rasa.sehaty.databinding.CustomTitleBarBinding;
import com.rasa.sehaty.databinding.HealthToolCalcLayoutBinding;
import com.rasa.sehaty.databinding.PragancyDueDateLayoutBinding;
import com.rasa.sehaty.model.HealthTool;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.parceler.Parcels;

import java.util.Calendar;

public class PDDCalculatorActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener {
    PragancyDueDateLayoutBinding binding;
    private HealthTool healthTool;
    private CustomTitleBarBinding titleBarBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDataBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.pragancy_due_date_layout);
        binding = (PragancyDueDateLayoutBinding) viewDataBinding;
        healthTool = Parcels.unwrap(getIntent().getParcelableExtra("health_tool"));
        setupActionBar();
        binding.pddDate.setOnClickListener(this);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_title_bar);

        View customView = getSupportActionBar().getCustomView();
        ViewDataBinding viewDataBinding = DataBindingUtil.bind(customView);
        titleBarBinding = (CustomTitleBarBinding) viewDataBinding;
        titleBarBinding.titleText.setText(healthTool.getToolName());
    }

    public void showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                PDDCalculatorActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOkColor(getResources().getColor(R.color.orange_color));
        dpd.setCancelColor(getResources().getColor(R.color.orange_color));
        dpd.setAccentColor(getResources().getColor(R.color.orange_color));
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        binding.pddValueText.setText("" + dayOfMonth + "/" + (++monthOfYear) + "/" + year);
    }

    @Override
    public void onClick(View v) {
        showDatePicker();
    }
}